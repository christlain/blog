---
title: Maandag 11 September
subtitle: Moodboard&Prototyping
date: 2017-09-11
tags: ["blog", "moodboard", "prototype", "app"]
---
Onze enquête had weinig succes, in totaal hebben er 6 op gereageerd en dat is niet representatief. We zijn verder gegaan met online informatie zoeken. 
Met de informatie die ik online heb gevonden, en een beetje uit de enquete heb kunnen halen is mijn moodboard vandaag af.
Mijn dit-ben-ik-profiel is ook af en het onderzoek is visueel gemaakt. 
We zijn begonnen aan het idee voor ons prototype. Ons concept is steeds meer vorm gaan geven. 

*Thuis*
Ik ben begonnen aan het prototypen, op school had ik niet de juiste materialen dus heb besloten dit thuis te maken. 
Ik heb eerst de verschillende beelden snel geschetst, toen heb ik gekozen welke ik uit ga vergroten.
Ik heb het home menu ontworpen en verschillende plaatjes in elkaar gezet, uitgeprint en getekend. 
Prototype is nog niet helemaal af, morgen ga ik eraan verder.
