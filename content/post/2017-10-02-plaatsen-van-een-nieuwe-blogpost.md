---
title: Maandag 2 Oktober
subtitle: Spelvorming
date: 2017-10-02
tags: ["blog", "briefing"]
---
Vandaag ben ik een uurtje later op school aangekomen omdat ik mij had verslapen. 
Toen ik op school aankwam hebben we nog even met z’n alle besproken wat nou het precieze spel word, het was nog allemaal onduidelijk. 
Vorige week hadden ik en Lorenzo wel het concept uitgekozen maar die moest natuurlijk helemaal worden uitgewerkt.
We hebben besloten om twee concepten te koppelen, concept 2 en 3. 
We hebben het zo gecombineerd dat concept 2 past bij enkele elementen uit concept 3 zoals de districten in Rotterdam.

Ik heb toen gelijk het spel, het concept en de spelregels uitgewerkt. 
Daarna zijn we met z’n alle het lijstje van deliverables afgegaan en nu was het duidelijk wat we nog allemaal moesten doen.
Ik heb toen onze moodboard samengevoegd in één complete moodboard, ik heb de creatieve technieken in een documenten gezet en uitgewerkt 
en heb het ontwerpproceskaart in een document gezet. Alles uploaden wij dan op dropbox. 