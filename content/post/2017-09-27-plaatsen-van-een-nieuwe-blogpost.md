---
title: Woensdag 27 September
subtitle: Via Web
date: 2017-09-27
tags: ["blog", "concept"]
---
Lorenzo en ik hebben een creatieve techniek toegepast, we zijn begonnen om het spel te bedenken door verschillende ideeën op te schrijven. 
Ik heb toen later op de dag uit die ideeën drie verschillenden concepten uitgewerkt. 
Ik heb toen in onze groepsapp de concepten gestuurd, aangezien Lorenzo en ik als enige op school waren, zo konden de andere dus meestemmen. 
Iedereen stemde op concept 2.  