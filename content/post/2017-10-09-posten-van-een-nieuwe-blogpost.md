---
title: Maandag 9 Oktober
subtitle: Big Changes
date: 2017-10-09
tags: ["blog"]
---

Ons spel mist het offline component en nu zitten wij een beetje in de knoop want in ons spel kunnen we dat moeilijk kwijt. 
We zitten nu te twijfelen om het spel flink aan te passen zodat dit wel kan. 
We zijn alle feedback aan het bekijken en proberen het zo aan te passen zodat we alle feedback meenemen.

**Middag**:
Nadat we Elske om een beetje raad hebben gevraagd kwamen allemaal ideeën in mijzelf op en ook bij mijn projectgenoten. 
We hebben besloten op een a4 al onze ideeën individueel op te schrijven en daarna elkaars ideeën aan te horen. 
We kwamen alle drie op een bordspel, die van Lorenzo en ik waren soortgelijk maar we zijn uiteindelijk voor Jorn’s idee gegaan en hebben daaruit verder gewerkt. 
