---
title: Maandag 4 September
subtitle: First Day
date: 2017-09-04
tags: ["blog", "firstday", "brainstorm"]
---
Vandaag hebben we onze eerste opdracht/briefing gekregen. 
Ik en Nicole hebben een brainstorm sessie gehad en kwamen snel op ideeën die wij gelijk noteerden, zoals een rad van fortuin.
Met wat ik heb meegekregen in het hoorcollege van Game Design, is dat spelers worden getriggerd om te spelen door beloningen dus daar dient het rad voor. 
Het proces van ideeën bedenken voor ons concept/spel ging heel snel,
al onze individuelen ideeën hebben we met z’n drieën uitgewerkt en nieuwe ideeën eraan toegevoegd. 
We hadden alle drie onze laptop niet mee dus wij konden verder weinig werk verrichten.