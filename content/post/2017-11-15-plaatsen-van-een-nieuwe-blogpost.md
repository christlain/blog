---
title: Woensdag 15 november
subtitle: Begin van kwartaal 2
date: 2017-11-15
tags: ["blog", "ColorfulMinds"]
---
Vandaag moesten we nieuwe groepjes vormen. 
Ik was al vrij snel een tweetal samen met Samantha en toen voegde Dominique en Beau zich bij ons aan.
We hadden ons groepje als één van de eerste gevormd en we kwamen alle 4 uit een verschillende categorie (belbinrollen). 
Toen we aan tafel zaten moesten we een naam gaan bedenken en toen kwam ik met Colorful Minds en hebben die toen ook gelijk opgeschreven. 
In de presentatie van Elske stond een planning voor de dag waar we aan konden werken, waaronder teamregels en afspraken etc. 
Later op de dag sloot Nikola zich aan bij ons groepje en hebben we toen met z’n alle de afspraken en gezamenlijke doelen opgeschreven. 
Voorderest hebben we vandaag allemaal een begin gemaakt aan de merkanalyse en hebben we besloten om allemaal individuele moodboards te maken, 
de helft maakte een van Paard en de andere helft maakte een moodboard van de concurrenten.